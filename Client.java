class Multiplicacion extends Operation
{
	public double operate(double a,double b)
	{
		return a*b;
	}
}

class Division extends Operation
{
	public double operate(double a,double b)
	{
		return a/b;
	}
}
class Arroba extends Operation
{
	public double operate(double a,double b)
	{
		return b*(a-2)/(a+b);
	}
}
class Arroba2 extends Operation
{
	public double operate(double a,double b)
	{
		Operation M=new Multiplicacion();
		Operation S=new Sum();
		Operation R=new Substraction();
		Operation D=new	Division();
		double Res=R.operate(a,2);
		double Su=S.operate(a,b);
		double Mul=M.operate(b,Res);
		return D.operate(Mul, Su);
		//Este metodo no es conveniente ya que tenemos que 
		//declarar muchos objetos nuevos.
	}
}
public class Client
{
	public static void main(String[] args)
	{
		Calc calc =new Calc();
		calc.register("*",new Multiplicacion());
		calc.register("/",new Division());
		calc.register("@",new Arroba());
		calc.register("@2",new Arroba2());
		double result= calc.operate("+",2,5);
		result= calc.operate("@2",10,3);
		System.out.println(result);
	}
}
Wilbert Alfonso Marroquin Caceres
