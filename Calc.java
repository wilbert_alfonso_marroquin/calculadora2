import java.util.*;
public class Calc 
{
	Map<String,Operation> operations;
	public Calc()
	{
		operations=new HashMap<String,Operation>();
		register("+",new Sum());
		register("-",new Substraction());
	}
	public void register(String operator,Operation operation)
	{
		operations.put(operator,operation);
	}
	public double operate(String operator,double a,double b)
	{
		Operation operation = operations.get(operator);
		return operation.operate(a,b);
	}

}
abstract class Operation
{
	public abstract double operate(double a,double b);
}
class Sum extends Operation
{
	public double operate(double a,double b)
	{
		return a+b;
	}
}
class Substraction extends Operation
{
	public double operate(double a,double b)
	{
		return a-b;
	}
}
Wilbert Alfonso Marroquin Caceres
